# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :phoenixnobrunchdiff,
  ecto_repos: [Phoenixnobrunchdiff.Repo]

# Configures the endpoint
config :phoenixnobrunchdiff, PhoenixnobrunchdiffWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "5Mc9ZK8dMPYgA88d14OPUTP0/WjKEQ9bLHIx30b1fijiFKxtVC7lGIDlx6jcxasT",
  render_errors: [view: PhoenixnobrunchdiffWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Phoenixnobrunchdiff.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
